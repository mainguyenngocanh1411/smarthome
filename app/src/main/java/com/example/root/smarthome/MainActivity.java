package com.example.root.smarthome;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private DatabaseReference mFirebaseDatabase_device;
    private DatabaseReference mFirebaseDatabase_info;
    private RadioGroup light1Group;
    private RadioButton light1On;
    private RadioButton light1Off;
    private RadioGroup light2Group;
    private RadioButton light2On;
    private RadioButton light2Off;
    private TextView tempValue;
    private TextView humiValue;
    static final int UdpServerPORT = 1111;
    static final int UdpClientPORT = 8888;
    private Button TV;
    Handler h = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        this.light1Group = findViewById(R.id.radio_1);
        this.light2Group = findViewById(R.id.radio_3);
        this.light1On = findViewById(R.id.light1On);
        this.light1Off = findViewById(R.id.light1Off);
        this.light2On = findViewById(R.id.light2On);
        this.light2Off = findViewById(R.id.light2Off);
        this.tempValue = findViewById(R.id.tempValue);
        this.humiValue = findViewById(R.id.humiValue);

        this.TV = findViewById(R.id.TV);
        //TODO
        /**
         * Condition
         *
         */
        //Air condition get from firebase
        TV.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                FirebaseDatabase mFirebaseInstance = FirebaseDatabase.getInstance();
                mFirebaseDatabase_device = mFirebaseInstance.getReference("devices");
                DatabaseReference acValue = mFirebaseInstance.getReference("devices").child("tv");
                acValue.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snap) {

                        SendToClient sendToClient = new SendToClient("tv", "192.168.1.11");
                        sendToClient.start();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }

                });
            }
        });
        FirebaseDatabase mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase_device = mFirebaseInstance.getReference("devices");
        DatabaseReference acValue = mFirebaseInstance.getReference("devices").child("tv");
        acValue.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //TODO
                //When firebase data change
                SendToClient sendToClient = new SendToClient("tv", "192.168.1.11");
                sendToClient.start();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        light1On.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                SendToClient sendToClient = new SendToClient("light1|1", "192.168.1.5");
                sendToClient.start();
                //updateDevice(true, "light 1", 1);
            }
        });
        light1Off.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                SendToClient sendToClient = new SendToClient("light1|0", "192.168.1.5");
                sendToClient.start();
                //updateDevice(true, "light 1", 0);
            }
        });
        light2On.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                SendToClient sendToClient = new SendToClient("light2|1", "192.168.1.8");
                sendToClient.start();
                //updateDevice(true, "light 2", 1);
            }
        });
        light2Off.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                SendToClient sendToClient = new SendToClient("light2|0", "192.168.1.8");
                sendToClient.start();
                //updateDevice(true, "light 2", 0);
            }
        });

        /* Listening change from firebase */
        mFirebaseDatabase_device = mFirebaseInstance.getReference("devices");
        mFirebaseDatabase_device.child("light 1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int changeValue = Integer.valueOf(Objects.requireNonNull(dataSnapshot.getValue()).toString());
                if (changeValue == 1) {
                    Log.d(TAG, "check on " + R.id.light1On);
                    SendToClient sendToClient = new SendToClient("light1|1", "192.168.1.5");
                    sendToClient.start();
                    light1Group.check(R.id.light1On);

                } else {
                    Log.d(TAG, "check of " + R.id.light1Off);
                    SendToClient sendToClient = new SendToClient("light1|0", "192.168.1.5");
                    sendToClient.start();
                    light1Group.check(R.id.light1Off);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        mFirebaseDatabase_device.child("light 2").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int changeValue = Integer.valueOf(Objects.requireNonNull(dataSnapshot.getValue()).toString());
                if (changeValue == 1) {
                    Log.d(TAG, "check on " + R.id.light2On);
                    SendToClient sendToClient = new SendToClient("light2|1", "192.168.1.8");
                    sendToClient.start();
                    light2Group.check(R.id.light2On);
                } else {
                    Log.d(TAG, "check of " + R.id.light2Off);
                    SendToClient sendToClient = new SendToClient("light2|0", "192.168.1.8");
                    sendToClient.start();
                    light2Group.check(R.id.light2Off);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        mFirebaseDatabase_info = mFirebaseInstance.getReference("info");
        mFirebaseDatabase_info.child("humi").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String changeValue = Objects.requireNonNull(dataSnapshot.getValue()).toString();
                humiValue.setText(changeValue);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mFirebaseDatabase_info.child("temperature").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String changeValue = Objects.requireNonNull(dataSnapshot.getValue()).toString();
                tempValue.setText(changeValue);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }


        });

        /* Listening change from arduino */
        //TODO

        Thread Arduino = new Thread() {
            @Override
            public void run() {
                int index;
                String temp = null;
                String humi = null;

                DatagramSocket serverSocket = null;
                try {
                    serverSocket = new DatagramSocket(UdpServerPORT);
                } catch (SocketException e) {
                    e.printStackTrace();
                }

                DatagramSocket clientSocket = null;
                {
                    try {
                        clientSocket = new DatagramSocket();
                    } catch (SocketException e) {
                        e.printStackTrace();
                    }
                }

                while (true) {

                    // nhận gói tin từ Client
                    byte[] receiveData = new byte[32];
                    DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                    try {
                        serverSocket.receive(receivePacket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String emptyRemoved = new String(receivePacket.getData());
                    String sentence = emptyRemoved.replaceAll("\u0000.*", "");


                    if (sentence.equals("current1|1")) {
                        updateDevice(true, "light 1", 1);

                    } else if (sentence.equals("current1|0")) {
                        updateDevice(true, "light 1", 0);

                    } else if (sentence.equals("current2|1")) {
                        updateDevice(true, "light 2", 1);

                    } else if (sentence.equals("current2|0")) {
                        updateDevice(true, "light 2", 0);

                    } else if (sentence.startsWith("Temp&Humi")) {
                        index = sentence.indexOf(';');
                        temp = sentence.substring(10, index);
                        humi = sentence.substring(index + 1, sentence.length());
                        Log.d("humi", "humi " + humi);

                        final String finalTemp = temp;
                        final String finalHumi = humi;
                        h.post(new Runnable() {
                            @Override
                            public void run() {
                                tempValue.setText(finalTemp);
                                humiValue.setText(finalHumi);
                                updateDevice(false, "humi", Integer.parseInt(finalHumi));

                                updateDevice(false, "temperature", Integer.parseInt(finalTemp));
                            }
                        });
                    }
                }
            }
        };
        loadTabs();
        Arduino.start();
    }

    public class SendToClient extends Thread {
        String command;
        String IP;
        byte[] sendData = new byte[1024];
        DatagramSocket clientSocket;

        {
            try {
                clientSocket = new DatagramSocket();
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }

        SendToClient(String command, String IP) {
            this.command = command;
            this.IP = IP;

        }

        public void run() {
            sendData = command.getBytes();
            // Tạo gói tin gửi đi thông qua IP address và port bất kì > 1023
            InetAddress IPAddress = null;
            try {
                IPAddress = InetAddress.getByName(IP);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, UdpClientPORT);
            try {
                clientSocket.send(sendPacket);
            } catch (IOException e) {
                e.printStackTrace();

            }
        }
    }


    private void updateDevice(boolean isDevice, String deviceName, int signal) {
        if (isDevice) {
            mFirebaseDatabase_device.child(deviceName).setValue(signal);
        } else {
            mFirebaseDatabase_info.child(deviceName).setValue(signal);
        }

    }

    public void loadTabs() {

        final TabHost tab = findViewById(R.id.tab_host);
        tab.setup();
        TabHost.TabSpec spec;
        spec = tab.newTabSpec("t1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Device");
        tab.addTab(spec);

        spec = tab.newTabSpec("t2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Info");
        tab.addTab(spec);
        tab.setCurrentTab(0);
    }
}